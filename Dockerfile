FROM node:stretch

ENV NODE_ENV production

WORKDIR /app

RUN apt-get update && \
    apt-get install build-essential -y

RUN npm i -g node-gyp
COPY package*.json yarn.lock ./
RUN yarn
COPY . .

CMD [ "yarn", "start" ]