module.exports = {
  presence: {
    offline: 0,
    online: 1,
    idle: 2,
    dnd: 3
  }
};
