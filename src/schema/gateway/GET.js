const yup = require("yup");

exports.description =
  "Fetches the URL of the gateway for real-time event dispatching";

exports.response = yup.object().shape({
  url: yup.string().url()
});
