const yup = require("yup");

const StringSchema = yup.string;
const invalid = null;

class UsernameSchema extends StringSchema {
  constructor() {
    super();
    this.withMutation(() => {
      this.transform(function(value, originalvalue) {
        if (this.isType(value)) return value;
        else return invalid;
      });
    });
  }

  _typeCheck(value) {
    return (
      super._typeCheck(value) ||
      (value.length >= 2 &&
        value.length < 32 &&
        value[0] != " " &&
        value[value.length - 1] != " " &&
        !value.match(require("./regex").evilCharacters))
    );
  }
}

module.exports = UsernameSchema;
