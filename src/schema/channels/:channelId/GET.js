const yup = require("yup");

exports.description = "Fetches the requested channel";

exports.response = yup.object().shape({
  id: yup.snowflake(),
  name: yup
    .string()
    .min(2)
    .max(32),
  guildId: yup.snowflake()
});
