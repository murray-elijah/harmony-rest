const yup = require("yup");

exports.description = "Returns a list of guilds viewable by the user";

exports.response = yup.array().of(require("./:guildId/GET.js").response);
