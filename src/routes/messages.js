const router = require("express").Router();
const permissionCheck = require("../permissionCheck");
const format = require("../format");
const r = require("../rethink");
const {dispatch} = (utils = require("../utils"));
const embedder = require("../embedder");
const crypto = require("crypto");

router.get("/", permissionCheck(["VIEW_CHANNEL"]), async function(req, res) {
  req.message.embeds = [];
  req.message.attachments = [];
  await Promise.all([
    r
      .table("embeds")
      .getAll(req.message.id, {index: "messageId"})
      .run(r.conn)
      .then(async embedsCursor => {
        for await (const embed of utils.cursorIterator(embedsCursor)) {
          req.message.embeds.push(embed);
        }
      }),

    r
      .table("attachments")
      .getAll(req.message.id, {index: "messageId"})
      .run(r.conn)
      .then(async attachmentCursor => {
        for await (const attachment of utils.cursorIterator(
          attachmentsCursor
        )) {
          req.message.attachments.push(attachment);
        }
      })
  ]);

  res.json(format.message(req.message));
});

router.delete(
  "/",
  permissionCheck(["VIEW_CHANNEL", "MESSAGE_CREATE"]),
  async function(req, res) {
    if (req.message.authorId != req.user.id) {
      if (
        !permissionCheck.rawPermissionCheck(req.totalPermissions, [
          "MANAGE_MESSAGES"
        ])
      ) {
        return await res.status(403)._json({
          permissionsNeeded: ["MANAGE_PERMISSIONS"]
        });
      }
    }
    await r
      .table("messages")
      .get(req.message.id)
      .delete()
      .run(r.conn);
    dispatch("MESSAGE_DELETE", {
      id: req.message.id,
      channelId: req.channel.id,
      guildId: req.channel.guildId
    });
    return res.json({success: true});
  }
);

router.patch(
  "/",
  permissionCheck(["VIEW_CHANNEL", "MESSAGE_CREATE"]),
  async function(req, res) {
    if (req.message.authorId != req.user.id) {
      return res
        .status(403)
        .json({message: "Only the owner of a message can edit it"});
    }
    const newMessageObject = {
      ...req.message,
      content: req.body.content
    };

    if (newMessageObject.content != req.message.content) {
      const embedPromises = new Set();

      const oldLinks = embedder.links(req.message.content);
      const newLinks = embedder.links(newMessageObject.content);
      // .filter(link => !oldLinks.includes(link));
      for (const link of newLinks) {
        if (!oldLinks.includes(link)) {
          embedPromises.add(
            // It's faster if we just have embed servers dispatch
            embedder.scrapeAndDispatch({
              link,
              messageId: req.message.id,
              channelId: req.message.channelId,
              guildId: req.channel.guildId
            })
          );
        }
      }

      const dispatchPromises = new Set();

      const removedLinks = [];

      for (const link of oldLinks) {
        if (!newLinks.includes(link)) {
          const linkHash = crypto
            .createHash("sha512")
            .update(Buffer.from(link, "utf8"))
            .digest("base64");

          const generatedId = `${req.message.id}-${linkHash}`;

          console.log("Deleting", generatedId);
          removedLinks.push(generatedId);
          dispatchPromises.add(
            dispatch("EMBED_DELETE", {
              messageId: req.message.id,
              channelId: req.message.channelId,
              guildId: req.channel.guildId,
              link,
              id: generatedId
            })
          );
        }
      }

      Promise.all([
        removedLinks.length &&
          r
            .table("embeds")
            .getAll(...removedLinks, {index: "id"})
            .delete()
            .run(r.conn),
        Promise.all(embedPromises),
        Promise.all(dispatchPromises)
      ]).then(allEmbeds => {
        console.log("Dispatched embed changes");
      });
    }

    await r
      .table("messages")
      .update(newMessageObject)
      .run(r.conn);
    const formatted = format.message(newMessageObject);
    dispatch("MESSAGE_UPDATE", {
      guildId: req.channel.guildId,
      channelId: req.channel.id,
      ...formatted
    });
    return res.json(formatted);
  }
);

const bodyParser = require("body-parser");
const minio = require("../minio");
const bignum = require("bignum");
const {Magic} = (mmm = require("mmmagic"));
const magic = new Magic(mmm.MAGIC_MIME_TYPE);
const env = require("../env");

router.use("/:attachmentId", async function(req, res, next) {
  if (req.user.id != req.message.authorId)
    return res
      .status(403)
      .json({message: "Only author can manipulate attachments"});
  console.log(req.params.attachmentId);
  const attachment = await r
    .table("attachments")
    .get(req.params.attachmentId)
    .run(r.conn);

  if (!attachment)
    return res.status(404).json({message: "Attachment not found (not)"});
  // 2 minute timeout...
  const limitSnowflake = bignum(Date.now() - env.epoch + 120000)
    .shiftLeft(22)
    .toString();
  // if (
  //   limitSnowflake.length > attachment.id.length ||
  //   limitSnowflake > attachment.id
  // ) {
  //   // Delete it since it's lingering...
  //   // TODO: We need a worker or something to delete these?
  //   await r
  //     .table("attachments")
  //     .get(req.params.attachmentId)
  //     .delete()
  //     .run(r.conn);
  //   return res.status(404).json({message: "Attachment not found (del'd)"});
  // }

  req.attachment = attachment;
  next();
});

router.put("/:attachmentId", async function(req, res) {
  if (req.attachment.uploaded)
    return res.status(404).json({message: "Cannot overwrite attachments!"});
  const [reqBody] = await Promise.all([
    new Promise((resolve, reject) => {
      const buffers = [];
      req.on("data", function(buffer) {
        buffers.push(buffer);
      });

      req.on("end", function() {
        resolve(Buffer.concat(buffers));
      });
      req.on("error", reject);
    }),

    minio.putObject(
      env.minio.bucketPrefix + "-message-attachments",
      req.attachment.id,
      req,
      undefined,
      {
        channelId: req.channel.id,
        messageId: req.message.id,
        guildId: req.channel.guildId
      }
    )
  ]);

  const mimeType = await new Promise((resolve, reject) =>
    magic.detect(reqBody, (err, res) => (err ? reject(err) : resolve(res)))
  );

  const attachment = {
    id: req.attachment.id,
    uploaded: true,
    messageId: req.message.id,
    mime: mimeType || "application/octet-stream",
    size: reqBody.length
  };

  await r
    .table("attachments")
    .update(attachment)
    .run(r.conn);

  const formatted = format.attachment({
    ...req.attachment,
    ...attachment
  });

  console.log("...", formatted);

  dispatch("ATTACHMENT_CREATE", {
    ...formatted,
    channelId: req.channel.id,
    guildId: req.channel.guildId,
    messageId: req.message.id
  });

  res.json(formatted);
});

module.exports = router;
