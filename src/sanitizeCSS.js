const css = require("css");

function cleanRules(oldRules) {
  const rules = [];
  for (const rule of oldRules) {
    if (rule.type == "rule") {
      rule.selectors = rule.selectors.map(
        selector =>
          selector.startsWith(".local-style-scope")
            ? selector
            : ".local-style-scope " + selector
      );
      rule.declarations = rule.declarations.map(cleanDeclaration);
      rules.push(rule);
    } else if (rule.type == "keyframes") {
      for (const keyframe of rule.keyframes) {
        keyframe.declarations = keyframe.declarations.map(cleanDeclaration);
      }
      rules.push(rule);
    } else if (rule.type == "charset" || rule.type == "@document") {
      rules.push(rule);
    } else if (rule.type == "media" || rule.type == "supports") {
      rule.rules = cleanRules(rule.rules);
      rules.push(rule);
    }
  }
  return rules;
}

function cleanDeclaration(declaration) {
  console.log(declaration);
  return declaration;
}

module.exports = {
  cleanRules
};
